FROM atlas/analysisbase:21.2.65

USER root

Add . /usr/multib/src/

RUN mkdir /usr/multib/build && \
    cd /usr/multib/src && \
    source /home/atlas/release_setup.sh && \
    git clone https://github.com/UCATLAS/xAODAnaHelpers.git && \
    mv ./MasterCMake CMakeLists.txt && \
    cd /usr/multib/build && \
    cmake ../src && \
    make && \ 
    mkdir /usr/multib/run


