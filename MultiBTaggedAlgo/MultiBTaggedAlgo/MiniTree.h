#ifndef AnalysisExample_MiniTree_H
#define AnalysisExample_MiniTree_H

#include "xAODAnaHelpers/HelpTreeBase.h"
#include "TTree.h"

class MiniTree : public HelpTreeBase
{

  private:
    bool m_firstEvent;

    std::vector<std::string> m_bTagWPs;
    std::vector< std::vector<float> > m_weight_btag;
    
    float m_yBoost;
    float m_yStar;
    float m_mjj;
    
    float m_weight;
    float m_weight_corr;
    float m_weight_xs;
    float m_weight_prescale;
    float m_weight_resonanceKFactor;

    float m_MHT;
    float m_MHTPhi;
    float m_MHTJVT;
    float m_MHTJVTPhi;

    std::vector< std::string > m_systSF_btag_names;

    float m_Insitu_Segs_response_E;
    float m_Insitu_Segs_response_pT;
    int m_punch_type_segs;

    std::vector<float> m_jet_constitScaleEta;
    std::vector<float> m_jet_emScaleE;
    std::vector<float> m_jet_emScaleEta;
    std::vector<float> m_jet_emScalePhi;

    std::vector<float> m_jet_recluster_pt;
    std::vector<float> m_jet_recluster_eta;
    std::vector<float> m_jet_recluster_phi;
    std::vector<float> m_jet_recluster_e;

  public:

    MiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, xAOD::TStore* store = nullptr);
    ~MiniTree();

    void AddEventUser( const std::string detailStr = "" );
    void AddJetsUser( const std::string detailStr = "", const std::string jetName = "jet" );
    void FillEventUser( const xAOD::EventInfo* eventInfo);
    void FillJetsUser( const xAOD::Jet* jet, const std::string jetName = "jet" );
    void ClearEventUser();
    void ClearJetsUser(const std::string jetName = "jet");

    void AddBtag(std::string bTagWPNames);
    void FillBtag( const xAOD::EventInfo* eventInfo );
    void ClearBtag();

};
#endif
