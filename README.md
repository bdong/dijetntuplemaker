This package is only tested on slc6.

How to compile:
=====================
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase  
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh  
mkdir dijetNtupleMaker   
cd dijetNtupleMaker  
mkdir src build  
cd src  
asetup AnalysisBase,21.2.65,here  
git clone ssh://git@gitlab.cern.ch:7999/bdong/dijetntuplemaker.git  
cd dijetntuplemaker  
git clone https://github.com/UCATLAS/xAODAnaHelpers.git  
cd ../../build  
cmake ../src  
make  
cd ../  
source build/${CMTCONFIG}/setup.sh  

How to run:
===================
Signal:  
xAH_run.py --config MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_signal.py  --files /file/you/downloaded --isMC --submitDir test --nevents 500 direct  
BKG:  
xAH_run.py --config MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_QCD.py --files /file/you/downloaded --isMC --submitDir test --nevents 500 direct  
data:  
xAH_run.py --config MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_Nominal_2017.py --files /file/you/downloaded --submitDir test --nevents 500 direct  

On grid: source run_grid_sig.sh

For testing new samples:
===================
Add samples DSID/Cross section info in MultiBTaggedAlgo/data/XsAcc_13TeV.txt
