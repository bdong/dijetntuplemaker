source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
voms-proxy-init -voms atlas
lsetup rucio
lsetup panda
xAH_run.py --inputRucio --inputList --files /afs/cern.ch/work/b/bdong/private/di-b/R21p2p51/src/MultiBJetTupleProduction/MultiBTaggedAlgo/scripts/sampleLists/SSMZprime_tottbar_mc16a.txt --config /afs/cern.ch/work/b/bdong/private/di-b/R21p2p51/src/MultiBJetTupleProduction/MultiBTaggedAlgo/data/config_MultiBTaggedAlgo_signal.py --force --submitDir gridRun/mc16a_sig --isMC prun --optGridOutputSampleName=user.bdong.%in:name[1]%.%in:name[2]%.%in:name[3]%.mc16a_4thAug --optGridNGBPerJob=2 
